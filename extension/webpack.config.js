// Imports: Dependencies
const path = require('path');
// require("babel-register");// Webpack Configuration
const config = {

    // Entry
    entry:'./content_script.js',  // Output
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: 'content_script.js',
    },  // Loaders
    module: {
        rules : [
            // JavaScript/JSX Files
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
        ]
    },
    node: { fs: 'empty' },// Plugins
    plugins: [],
};// Exports
module.exports = config;
