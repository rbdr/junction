'use strict';

const internals = {
  peers: {},

  createAudioElement(source) {

    const audioElement = document.createElement('audio');
    audioElement.setAttribute('class', 'junction-call-audio');
    audioElement.autoplay = 'autoplay';

    // WE WILL NOT LOSE TADA SUPPORT
    if (typeof source === 'string') {
      audioElement.src = source;
    }
    else {
      audoElement.srcObject = source;
    }

    document.querySelector('body').appendChild(audioElement);

    return audioElement;
  }
};

module.exports = {
  add(id, source) {

    internals.peers[id] && this.remove(id);
    internals.peers[id] = internals.createAudioElement(source)
  },

  remove(id) {

    internals.peers[id] && internals.peers[id].remove();
    delete internals.peers[id];
  },

  count() {

    return Object.keys(internals.peers).length;
  },

  reset() {

    internals.peers = {};
    document.querySelectorAll('.junction-call-audio').forEach((audioElement) => audioElement.remove());
  }
};
